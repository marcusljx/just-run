# just-run
experiment on using Knative as a test-farm

## Why Knative?
- scale-to-zero compute
- serverless

but most importantly, because it's new and I like new technologies.

## My Environment
`Linux 4.15.0-39-generic (Ubuntu 18.04)`

## Steps
### 0: Setup
#### Local Tools
##### `minikube` with `kvm2` driver
```bash
# minikube
curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.30.0/minikube-linux-amd64 && chmod +x minikube && sudo cp minikube /usr/local/bin/ && rm minikube

# kvm2 driver
sudo apt install -yy libvirt-clients libvirt-daemon-system qemu-kvm
sudo usermod -a -G libvirt $(whoami)  ## if you're on an older
newgrp libvirt

# Note: When I executed the above, the last command(`newgrp libvirt`) failed with 
#   cannot execute zsh: No such file or directory
#
# After some intense digging, I found out that this is due to my environment `$SHELL` variable 
# being set to `zsh`(instead of `/bin/zsh`) during my installation of ZSH long ago.
```

##### `kubectl` and `helm`
```bash
sudo snap install --classic kubectl
sudo snap install --classic helm
```

##### `istioctl` and libraries
```bash
# get istio tools (optional: istio installation already exists in `/external/istio-1.0.4`)
curl -L https://git.io/getLatestIstio | sh -
export ISTIO_HOME="${PWD}/istio-1.0.4"
export PATH="$PATH:${ISTIO_HOME}/bin"
```


#### Kubernetes cluster setup
##### Start minikube with the proper setup
```bash
# (optional) stop and delete fully any existing minikube installation 
minikube stop && minikube delete
rm -rf ${HOME}/.minikube

# start fresh minikube cluster
minikube start --memory=8192 --cpus=4 \
  --kubernetes-version=v1.11.3 \
  --vm-driver=kvm2 \
  --bootstrapper=kubeadm \
  --extra-config=apiserver.enable-admission-plugins="LimitRanger,NamespaceExists,NamespaceLifecycle,ResourceQuota,ServiceAccount,DefaultStorageClass,MutatingAdmissionWebhook"
#  --extra-config=controller-manager.cluster-signing-cert-file="/var/lib/localkube/certs/ca.crt" \  # possibly needed only in PRD; remember to add '\' to EOL above 
#  --extra-config=controller-manager.cluster-signing-key-file="/var/lib/localkube/certs/ca.key" \   # possibly needed only in PRD

minikube addons enable registry-creds
```

##### install `helm` + `tiller` + `istio`
```bash
kubectl apply -f ${ISTIO_HOME}install/kubernetes/helm/helm-service-account.yaml
# kubectl get all -n kube-system

helm init --service-account tiller
helm install ${ISTIO_HOME}/install/kubernetes/helm/istio --name istio --namespace istio-system
# kubectl get all -n istio-system
```

##### install Knative
```bash
curl -L https://github.com/knative/serving/releases/download/v0.2.1/release-lite.yaml \
  | sed 's/LoadBalancer/NodePort/' \
  | kubectl apply --filename -

# kubectl get all -n knative-serving
```

##### deploy container as batch job
```bash
# from repository root
kubectl apply -f ./deploy/fibonacci.yml

# view kubernetes status
kubectl describe jobs/fibonacci

# view logs (get pod name from above command)
kubectl logs fibonacci-8qdgr
```
package main

import (
	"flag"
	"fmt"
	"log"
	"math/big"
	"os"
	"strconv"
)

var (
	stdoutlogger = log.New(os.Stdout, "STDOUT", log.LstdFlags)
	stderrlogger = log.New(os.Stderr, "STDERR", log.LstdFlags)
)

func init() {
	flag.Usage = func() {
		fmt.Printf("Usage: %s N\n", os.Args[0])
		flag.PrintDefaults()
	}
}

func main() {
	flag.Parse()
	if len(os.Args) != 2 {
		flag.Usage()
		stderrlogger.Printf("os.Args : %v", os.Args)
		os.Exit(1)
	}

	n, err := strconv.Atoi(os.Args[1])
	if err != nil {
		stdoutlogger.Printf("error : strconv.Atoi : %s\n", err)
		os.Exit(1)
	}

	if n < 0 {
		stdoutlogger.Printf("error : expected positive number but got %d\n", n)
	}
	stderrlogger.Printf("calculating fib(%d)", n)

	fmt.Println(Fibonacci(n))
}

// Fibonacci returns the nth fibonacci number
func Fibonacci(n int) *big.Int {
	switch n {
	case 0:
		return big.NewInt(0)
	case 1, 2:
		return big.NewInt(1)
	}
	// n >= 3
	arr := make([]*big.Int, n)
	copy(arr, []*big.Int{big.NewInt(1), big.NewInt(1)})

	for i := 2; i < n; i++ {
		arr[i] = big.NewInt(0).Add(arr[i-1], arr[i-2])
		stderrlogger.Printf("%6d : %40d", i-1, arr[i])
	}
	return arr[n-1]
}

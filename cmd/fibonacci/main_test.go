package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestFibonacci(t *testing.T) {
	type testcase struct {
		input    int
		expected int64
	}
	for _, c := range []testcase{
		{input: 3, expected: 2},
		{input: 4, expected: 3},
		{input: 5, expected: 5},
		{input: 6, expected: 8},
		{input: 7, expected: 13},
		{input: 8, expected: 21},
		{input: 9, expected: 34},
		{input: 10, expected: 55},
		{input: 11, expected: 89},
		{input: 12, expected: 144},
		{input: 13, expected: 233},
		{input: 14, expected: 377},
		{input: 15, expected: 610},
		{input: 16, expected: 987},
		{input: 17, expected: 1597},
		{input: 18, expected: 2584},
		{input: 19, expected: 4181},
		{input: 20, expected: 6765},
		{input: 21, expected: 10946},
		{input: 22, expected: 17711},
		{input: 23, expected: 28657},
		{input: 24, expected: 46368},
		{input: 25, expected: 75025},
		{input: 26, expected: 121393},
		{input: 27, expected: 196418},
		{input: 28, expected: 317811},
		{input: 29, expected: 514229},
		{input: 30, expected: 832040},
		{input: 31, expected: 1346269},
		{input: 32, expected: 2178309},
		{input: 33, expected: 3524578},
		{input: 34, expected: 5702887},
		{input: 35, expected: 9227465},
		{input: 36, expected: 14930352},
		{input: 37, expected: 24157817},
		{input: 38, expected: 39088169},
		{input: 39, expected: 63245986},
		{input: 40, expected: 102334155},
		{input: 41, expected: 165580141},
		{input: 42, expected: 267914296},
		{input: 43, expected: 433494437},
		{input: 44, expected: 701408733},
		{input: 45, expected: 1134903170},
		{input: 46, expected: 1836311903},
		{input: 47, expected: 2971215073},
		{input: 48, expected: 4807526976},
		{input: 49, expected: 7778742049},
		{input: 50, expected: 12586269025},
		{input: 51, expected: 20365011074},
		{input: 52, expected: 32951280099},
		{input: 53, expected: 53316291173},
		{input: 54, expected: 86267571272},
		{input: 55, expected: 139583862445},
		{input: 56, expected: 225851433717},
		{input: 57, expected: 365435296162},
	} {
		tc := c
		t.Run(fmt.Sprintf("%d", tc.input), func(tt *testing.T) {
			actual := Fibonacci(tc.input)
			require.Equal(tt, tc.expected, actual.Int64())
		})
	}
}
